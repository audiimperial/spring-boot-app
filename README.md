# Spring Boot User Service Application

## Description

This project is a Spring Boot application that provides a backend API for user registration, editing, reading, and (soft) deleting single or multiple users. The application is packaged with Docker and sends a welcome email upon successful registration.

## Requirements

- Docker
- Docker Compose
- Maven

## How to Run

1. Clone the repository:
    ```sh
    git clone https://gitlab.com/audiimperial/spring-boot-app.git
    cd spring-boot-app
    ```

2. Run the application:
    ```sh
    run.bat
    ```

3. Access the application:
    - User Service API: `http://localhost:9090/users`
    - MailDev (Fake SMTP Server): `http://localhost:1080`

## API Endpoints

- `POST /users` - Register a new user
- `GET /users/{id}` - Get user by ID
- `GET /users` - Get all users
- `PUT /users` - Update user
- `DELETE /users/{id}` - Delete user by ID

## Docker Compose

The application uses Docker Compose to manage multiple services:
- `user-service`: The Spring Boot user service application
- `smtp`: MailDev, a fake SMTP server for testing email functionality

## REST API Documentation

The REST API documentation is available via Swagger (if enabled) at `http://localhost:9090/swagger-ui/index.html`.

## Testing

Run the tests with Maven:
```sh
mvn test
