rem Build the project
cmd /c mvn clean install -DskipTests

rem Build Docker images
cmd /c docker-compose build

rem Start the application
cmd /c docker-compose up
