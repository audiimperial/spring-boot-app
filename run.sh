#!/bin/bash

# Build the project
mvn clean install -DskipTests

# Build Docker images
docker-compose build

# Start the application
docker-compose up
